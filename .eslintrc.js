module.exports = {
	"parser": "babel-eslint",
	"env": {
		"browser": true,
		"es6": true,
		"node": true
	},
	"extends": [
		"abro",
		"abro/react"
	],
	"parserOptions": {
		"ecmaFeatures": {
			"experimentalObjectRestSpread": true,
			"jsx": true
		},
		"sourceType": "module"
	},
	"plugins": [
		"react"
	],
	"rules": {
		"indent": [
			"error",
			"tab",
			{
				"SwitchCase": 1
			}
		],
		"linebreak-style": [
			"error",
			"unix"
		],
		"quotes": [
			"error",
			"single"
		],
		"semi": [
			"error",
			"always"
		],
		/* Advanced Rules*/
		"no-unused-expressions": "warn",
		"no-useless-concat": "warn",
		"block-scoped-var": "error",
		"consistent-return": "error"
	}
};
