import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class Layout extends Component {
	static propTypes = {
		title: PropTypes.string,
		children: PropTypes.oneOfType([PropTypes.array, PropTypes.string, PropTypes.object]),
	};

	render() {
		const {title, children} = this.props;

		return (
			<div>
				{children}
			</div>
		);
	}
}
