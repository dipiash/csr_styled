/**
 * Error action creator
 *
 * @param {String} type Action Type
 * @param {Number} status Error status
 * @param {String} message Error message
 * @param {String} description Error description
 *
 * @return {Object} {{type: *, payload: {status: *, message: *}}}
 */
const errorActionCreator = (type, status, message, description) => ({
	type,
	payload: {status, message, description},
});

export default errorActionCreator;
