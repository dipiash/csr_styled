import Notification from '@zfm/react/lib/antd/notification';

/**
 * Error middleware
 *
 * @return {Function} {function(*): function(*=)}
 */
export const errorMiddleware = () => next => action => {
	const checkError = action.type.indexOf('_ERROR') !== -1;

	if (checkError) {
		Notification.error({
			message: action.payload.message,
			description: action.payload.description,
			duration: 5,
		});
	}

	return next(action);
};
