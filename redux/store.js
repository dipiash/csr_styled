import {applyMiddleware, createStore, combineReducers} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';

const rootReducer = combineReducers({
	c: () => ({})
});

const initStore = (initialState = {}) => createStore(
	rootReducer,
	initialState,
	composeWithDevTools(applyMiddleware(thunkMiddleware))
	// process.env.NODE_ENV === 'production'
	// 	? applyMiddleware(thunkMiddleware)
	// 	: composeWithDevTools(applyMiddleware(thunkMiddleware)),
);

export default initStore;
